package com.example.apiserverb;

import com.example.apiserverb.feign.FeignService;
import com.example.apiserverb.feign.ServiceA;
import com.example.apiserverb.feign.ServiceA2;
import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/web/b")
public class ControllerB {

//    @Autowired
//    ServiceA serviceA;

    @Autowired
    FeignService feignService;

    @GetMapping("/get")
    public String get(){
        return "b-b";
    }

    @GetMapping("/geta")
    public Object geta(){
        return feignService.getServiceA().geta();
    }

    @GetMapping("/geta2")
    public Object geta2() throws URISyntaxException {
        return feignService.getServiceA2().geta(new URI("http://127.0.0.1:8084"));
    }
}
