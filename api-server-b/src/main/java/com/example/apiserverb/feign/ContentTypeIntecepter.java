package com.example.apiserverb.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * @author will.tuo
 * @date 2022/12/9 15:21
 * @description 无
 */
public class ContentTypeIntecepter implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        template.header("ContentType","application/json");
    }
}
