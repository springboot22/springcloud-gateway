package com.example.apiserverb.feign;

import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author will.tuo
 * @date 2022/12/9 14:39
 * @description 无
 */
@FeignClient(value = "server-a")
public interface ServiceA {
    @GetMapping("/api/web/a/get")
    public String geta();
}
