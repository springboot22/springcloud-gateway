package com.example.apiserverb.feign;

import feign.Client;
import feign.Feign;
import feign.Request;
import feign.RequestTemplate;
import feign.Target;
import feign.Target.HardCodedTarget;
import feign.codec.Decoder;
import feign.codec.Encoder;
import javax.annotation.sql.DataSourceDefinition;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
//import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.cloud.openfeign.loadbalancer.FeignBlockingLoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2022/12/9 15:38
 * @description 无
 */
@Data
@Configuration
@Import(FeignClientsConfiguration.class)
public class FeignService {

    private ServiceA2 serviceA2;

    @Autowired
    private ServiceA serviceA;

//    @Autowired
//    private FeignBlockingLoadBalancerClient client;



    @Autowired
    public FeignService(Decoder decoder, Encoder encoder) {
        serviceA2 = Feign.builder().encoder(encoder).decoder(decoder).requestInterceptor(new ContentTypeIntecepter())
                .target(Target.EmptyTarget.create(ServiceA2.class));
//        serviceA = Feign.builder().target(new Target.HardCodedTarget<>(ServiceA.class,"server-a","http://server-a"));
    }
}
