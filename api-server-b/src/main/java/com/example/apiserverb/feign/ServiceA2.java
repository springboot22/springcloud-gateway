package com.example.apiserverb.feign;

import feign.RequestLine;
import java.net.URI;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author will.tuo
 * @date 2022/12/9 14:39
 * @description 无
 */
@FeignClient(name = "server-a2",configuration = {ContentTypeIntecepter.class})
public interface ServiceA2 {
    @RequestLine("GET /api/web/a/get")
    public String geta(URI uri);
}
