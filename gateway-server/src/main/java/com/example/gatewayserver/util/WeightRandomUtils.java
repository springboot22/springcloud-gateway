package com.example.gatewayserver.util;

/**
 * @author will.tuo
 * @date 2022/12/8 15:16
 * @description 无
 */

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * 随机工具类，使用权重的集合Map构建随机元数据对象，权重算法取自：
 * @see <a href="https://www.ctolib.com/topics-61571.html">...</a>
 * 比如：
 * 我们有3个url地址，他们的权重分别为1,2,3现在我们利用RandomUtil来根据权重随机获取url：
 *
 * <pre>map.put(url1, 1);
 * map.put(url2, 2);
 * map.put(url3, 3);
 * RandomMeta<String, Integer> md = WeightRandomUtils.buildWeightMeta(map);
 * String weightRandomUrl = md.random();</pre>
 */
public class WeightRandomUtils {
    public static <T> WeightMeta<T> buildWeightMeta(final Map<T, Integer> weightMap) {
        if(weightMap.isEmpty()){
            return null;
        }
        final int size = weightMap.size();
        Object[] nodes = new Object[size];
        int[] weights = new int[size];
        int index = 0;
        int weightAdder = 0;
        for (Map.Entry<T, Integer> each : weightMap.entrySet()) {
            nodes[index] = each.getKey();
            weights[index++] = (weightAdder = weightAdder + each.getValue());
        }
        return new WeightMeta<T>((T[]) nodes, weights);
    }
}
