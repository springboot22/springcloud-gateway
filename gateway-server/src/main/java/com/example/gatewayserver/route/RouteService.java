package com.example.gatewayserver.route;

import org.springframework.cloud.gateway.route.RouteDefinition;

/**
 * @author will.tuo
 * @date 2022/12/8 13:12
 * @description 无
 */
public interface RouteService {
    /**
     * 更新路由配置
     *
     * @param routeDefinition
     */
    void update(RouteDefinition routeDefinition);

    /**
     * 添加路由配置
     *
     * @param routeDefinition
     */
    void add(RouteDefinition routeDefinition);
}
