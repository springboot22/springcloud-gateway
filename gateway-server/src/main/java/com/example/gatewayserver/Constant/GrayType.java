package com.example.gatewayserver.Constant;

/**
 * @author will.tuo
 * @date 2022/12/8 16:23
 * @description 无
 */
public enum GrayType {
    /**
     * 以下被注释的，可能在场景上有所不同，不一定能全部兼容，先放着再说
     */
//    STABLE("stable", "稳定版本模式"),
//    GRAYTEST("grayTest", "灰度测试模式"),
//    ROLLBACK("rollback", "回滚模式"),
//    ROLLINGPUBLISH("rollingPublish", "滚动发布模式"),
//    STABLE_ONLY_MODE("stable", "只访问稳定版本服务"),
//    GRAY_ONLY_MODE("gray", "只访问灰度版本服务"),
    ALL_MODE("all", "访问所有版本服务");


    private String code;
    private String desc;

    GrayType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getDesc(String code) {
        for (GrayType c : GrayType.values()) {
            if (code.equals(c.getCode())) {
                return c.getDesc();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
