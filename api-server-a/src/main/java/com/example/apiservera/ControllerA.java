package com.example.apiservera;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/web/a")
public class ControllerA {
    @GetMapping("/get")
    public String get(){
        System.out.println("a-a");
        return "a-a";
    }
}
