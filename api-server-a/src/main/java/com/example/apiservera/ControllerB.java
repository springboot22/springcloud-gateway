package com.example.apiservera;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/web/b")
public class ControllerB {
    @GetMapping("/get")
    public String get(){
        return "a-b";
    }
}
