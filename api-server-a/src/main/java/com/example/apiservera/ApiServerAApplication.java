package com.example.apiservera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ApiServerAApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiServerAApplication.class, args);
    }

}
