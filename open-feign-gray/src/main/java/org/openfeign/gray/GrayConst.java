package org.openfeign.gray;

/**
 * @author will.tuo
 * @date 2022/12/14 16:51
 * @description 无
 */
public interface GrayConst {
    String HEADER_REQUEST_MODE = "grayMode";
    String HEADER_REQUEST_VERSION = "grayVersion";
}
