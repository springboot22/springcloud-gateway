package org.openfeign.gray.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author will.tuo
 * @date 2022/12/14 16:54
 * @description 无
 */
@Slf4j
public class GrayHeaderRequestInterceptor implements RequestInterceptor {
    /**
     * 要进行透传所有header的名称
     */
    @Value("${gray.header.names:targetVersion,requestMode,UserId}")
    private String[] headerNames;
    @Override
    public void apply(RequestTemplate template) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            // 获取不到原请求，可能是对feign的调用为异步导致
            log.error(
                    "[feign gray extension] header pass failed: RequestContextHolder.getRequestAttributes() is null");
            return;
        }
        HttpServletRequest oldRequest = ((ServletRequestAttributes)requestAttributes).getRequest();
        passHeader(oldRequest, template);
    }

    /**
     * header透传
     *
     * @param oldRequest
     * @param template
     */
    private void passHeader(HttpServletRequest oldRequest, RequestTemplate template) {
        String userId = oldRequest.getHeader("UserId");
        for (String headerName : headerNames) {
            String headerValue = oldRequest.getHeader(headerName);
            if (headerValue == null) {
                log.info("[feign gray extension] header ignore: target header value is null, headerName = {}, requestUrl = {}, userId={}",
                        headerName, oldRequest.getRequestURL(), userId);
            } else {
                log.info("[feign gray extension] header passed: headerName = {}, headerValue = {}, requestUrl = {}, userId={}", headerName,
                        headerValue, oldRequest.getRequestURL(), userId);
                template.header(headerName, headerValue);
            }
        }
    }

}
